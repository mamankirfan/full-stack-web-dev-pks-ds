<?php

trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        echo "{$this->nama} sedang {$this->keahlian}";
    }
}

abstract class Fight{

    use Hewan;

    public $attackPower;
    public $defencePower;

    public function serang($hewan){
        echo "{$this->nama} sedang menyerang {$hewan->nama} <br>";
        $this->diserang($hewan);
    }

    public function diserang($hewan){
        echo "{$hewan->nama} sedang diserang {$this->nama} <br>" ;
        $this->darah = $this->darah - ($hewan->attackPower / $this->defencePower);

    }

    abstract public function getInfoHewan();
}

class Elang extends Fight{
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan : Elang";
        echo "<br>";
        echo "Nama = {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "attack : {$this->attackPower}";
        echo "<br>";
        echo "deffense : {$this->defencePower}";
        echo "<br>";
        echo "<br>";

    }
}

class Harimau extends Fight{
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan : Harimau";
        echo "<br>";
        echo "Nama = {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "attack : {$this->attackPower}";
        echo "<br>";
        echo "deffense : {$this->defencePower}";
        echo "<br>";
        echo "<br>";


    }
}


$harimau = new Harimau("Harimau sumatra");   
$harimau->getInfoHewan();

$elang = new Elang("Elang Jawa");   
$elang->getInfoHewan();

$harimau->serang($elang); 
echo "<br>";
?>